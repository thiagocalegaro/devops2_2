//Thiago Calegaro Martin e Roger Couto

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct aplicativo {
    char nome[50];
    struct aplicativo *prox;
};
typedef struct aplicativo newapp;

struct  criar_tela {
    int n;//numero da tela
    int cont;//conta quantos apps tem a tela
    struct criar_tela *prox;
    struct criar_tela *ant;
    newapp *aplicativo; 
};
typedef struct criar_tela newtela;

newtela *add_app(newtela *tela){
	newtela *aux = tela;
	char nomeapp[50]; //onde sera armazenado o valor a ser gravado no campo info do app
	newapp *app;
	app = (newapp*) malloc (sizeof(newapp)); //aloca memória
	printf("\nDigite o aplicativo que deseja instalar:\n");
	gets(nomeapp);
	strlwr(nomeapp); //define todas as letras como minúsculas
	strcpy(app->nome, nomeapp);
	app->prox = NULL;
	system("cls");
	
	if(tela == NULL){//na primeira execução do código, cria uma nova tela
		newtela *novo;
		novo = (newtela*) malloc (sizeof(newtela));//aloca memória
		novo->n = 1; 
		novo->prox = NULL;
		novo->ant = NULL;
		novo->cont = 1;//contador igual 1, pq app já foi criado
		novo->aplicativo = app;//aponta para o app, usado para acessar os apps atraves da tela
		return novo; //retorna o aplicativo criado
	}	
	
	
	if(aux != NULL && aux->ant == NULL && aux->prox == NULL && aux->cont == 0){//caso tenha uma unica tela sem apps aponta para o primeiro app
		aux->aplicativo = app;
		aux->n = 1; 
		aux->cont = 1;
	}
	
			
	else if( aux->cont < 4){//se a quantidade de apps não for atingida
		aux->cont++;
		  if(aux->cont == 2){
			aux->aplicativo->prox = app;
		}
		else{
			newapp *p;
			p = aux->aplicativo;
			while( p->prox != NULL){
				p = p->prox;
			}
	        p->prox = app;
		}
	}
	
	else{ //se atingir a quantidade maxima de apps na tela, cria uma nova tela
		newtela *novo;
		novo = (newtela*) malloc (sizeof(newtela));//aloca memória
		novo->n = aux->n + 1; 
		novo->prox = NULL;
		novo->ant = aux;
		novo->cont = 1;//contador igual a 1 pois ao rodar este parte, um app ja estara criado
		novo->aplicativo = app;//aponta para o app, usado para acessar os apps atraves da tela
		aux->prox = novo;
		tela = tela->prox; //tela atual passa a ser a nova criada
	}

	return tela;
}


void visualizar(newapp *p) { //função para mostrar os apps da tela atual
    if (p != NULL) {
        puts(p->nome);
        visualizar(p->prox);
    }
}

void opcoes(){ //printa as opções
    printf("\n1 - Instalar app");
    printf("\n2 - Desinstalar app");
    printf("\n3 - Proxima tela");
    printf("\n4 - Tela anterior");
	printf("\n5 - Encerrar sistema\n");
}


newtela *remover_app(newtela *tela){
	
	char nomeapp[50];
	newtela *aux = tela;
	newapp *p = aux->aplicativo;
	newapp *ant = NULL;
	printf("\nNome do app que deseja desinstalar: ");
	gets(nomeapp);
	strlwr(nomeapp);//defina todas as letras como minúsculas
	system("cls");
	
	while(p != NULL && strcmp(p->nome,nomeapp) != 0){//chega ao fim da lista ou encontra o nome do app
		ant = p;
		p = p->prox;
	}
	
	if (p == NULL){
		printf("\nEsse app nao existe na tela atual\n");
		return tela;
	}
	
	if (ant == NULL){       
		aux->aplicativo = p->prox;
		aux->cont--;		
	}
	
	else if(ant!=NULL){
			ant->prox = p->prox;
			aux->cont--;
		 }	
	
	
	if(aux->cont == 3 && aux->prox != NULL){//organiza os apps para nao haver telas com espaços vazios

			while(aux->cont < 4 && aux->prox != NULL){//muda de tela conforme organiza
					
				newapp *p1 = aux->aplicativo;//aponta para o primeiro app da tela atual
				newapp *p2 = aux->prox->aplicativo;//aponta para o primeiro app da próxima tela
				
				while(p1->prox != NULL){//vai para o fim da lista
					ant = p1;
					p1 = p1->prox;
				}
					
				aux->cont++;
				p1->prox = p2;  //prox app recebe o que está em p2
				aux->prox->aplicativo = p2->prox; //primeiro app da próxima tela recebe o prox app de p2
				aux->prox->cont--;
				p2->prox = NULL; //último app não aponta pra ninguém
				aux = aux->prox; //vai para a prox tela
			}
	
		while(aux->prox != NULL){
			aux = aux->prox;//vai para a última tela
		}
	}
	
	if(aux->cont == 0){ //apaga tela vazia
		
		if(tela->ant != NULL && tela->cont == 0 ){//retorna para tela anterior após excluir app se a tela estiver vazia
			tela = tela->ant;
		}
		
		if(aux->prox == NULL && aux->ant == NULL){//mantém a primeira tela, mesmo estando vazia 
			return tela;
		}
		
		else{
				aux->ant->prox = aux->prox;
				free(aux); //desaloca o tela vazia da memória
			}
	}
	free(p); //libera espaço alocado em p
	return tela;
}

int main() {
    
    newtela *tela;
	tela = NULL;
    int opcao;
    do{
    	if(tela !=  NULL){
    		printf("Tela atual: %d\n\nApps:\n", tela->n);
			visualizar(tela->aplicativo);
		}
		else{
			printf("Bem vindo!\n");
		}
		
        opcoes();
        printf("\nDigite a operacao que deseja realizar: ");
        scanf("%d", &opcao);
    	fflush(stdin);
    	
    	switch(opcao){
        
        	case 1:
				if(tela != NULL){
			 		while(tela->prox != NULL){//insere os elementos sempre no final da lista, usado apenas se a tela for diferente de NULL, se a tela for nula, não tem como receber tela->prox
						tela = tela->prox;
					}	
					tela = add_app(tela);
				}	
	       		else{
	       			tela = add_app(tela);
		   		}
        	break;
		
			case 2:
        		if(tela == NULL || tela->cont == 0){//impede  excluir apps caso ela esteja vazia
        			system("cls");
					printf("\nOperacao invalida, nenhum app existente\n");
				}
				else{
					tela = remover_app(tela);
				}
			break;
		
			case 3:
				if(tela->prox != NULL){//condição que impede que a tela seja pulada caso o campo proximo seja NULL
					system("cls");
					tela = tela->prox;
				}
				else{
					system("cls");
					printf("Proxima tela nao existe\n");
				}	
			break;
		
			case 4:
				if(tela->ant != NULL){//verifica se possui tela anterior
					system("cls");
					tela = tela->ant;
				}
				else{
					system("cls");
					printf("Tela anterior nao existe\n");
				}				
			break;
        
        	case 5: //encerra o programa
				system("cls");
				printf("\nOperacoes encerradas!\nObrigado por utilizar nosso sistema");
			break;
			
			default:
				system("cls");//mensagem para quando for digitado um valor sem opção
				printf("\nOpcao Invalida.\n\n");
			break;
		}
        
    }while(opcao != 5);  
    
    free(tela);
	
	return 0;
}
